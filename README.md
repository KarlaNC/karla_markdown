# 1.- El objetivo es repasar los comandos que permiten utilizar Git para compartir un desarrollo de software:
***
***Temas y Actividades***
## **Hoja03 Markdown 02**
***
#### Pasos a seguir : 


1.	Crea un repositorio en GitLab o GitHub llamado TUNOMBRE_markdown

	![imagen](imagenes/Hoja02/1-1.PNG "Repositorio en GitLab") 

2.	Clona el repositorio en local  

	![imagen](imagenes/Hoja02/2-1.PNG "Acceder a la carpeta donde queremos tener el repositorio") 
	![imagen](imagenes/Hoja02/2-2.PNG "Clonarlo") 

3.	Crea en tu repositorio local un documento README.md Nota: en este documento tendrás que ir poniendo los comandos que has tenido que utilizar durante todos los ejercicios y las explicaciones y capturas de pantalla que consideres necesarias

	![imagen](imagenes/Hoja02/3-1.PNG "") 
	![imagen](imagenes/Hoja02/3-2.PNG "") 
	![imagen](imagenes/Hoja02/3-3.PNG "") 
	![imagen](imagenes/Hoja02/3-4.PNG "") 

4.	Añadir al README.md los comandos utilizados hasta ahora y hacer un commit inicial con el mensaje “Primer commit de TUNOMBRE”.

	![imagen](imagenes/Hoja02/4-1.PNG "") 

5.	Sube los cambios al repositorio remoto.

	![imagen](imagenes/Hoja02/5-1.PNG "") 

6.	Crear en el repositorio local un fichero llamado privado.txt. Crear en el repositorio local una carpeta llamada privada.

	![imagen](imagenes/Hoja02/6-1.PNG "") 
	![imagen](imagenes/Hoja02/6-2.PNG "") 
	![imagen](imagenes/Hoja02/6-3.PNG "") 
	![imagen](imagenes/Hoja02/6-4.PNG "") 
	![imagen](imagenes/Hoja02/6-5.PNG "") 

7.	Realizar los cambios oportunos para que tanto el archivo como la carpeta sean ignorados por git. 

	![imagen](imagenes/Hoja02/7-1.PNG "") 
	![imagen](imagenes/Hoja02/7-2.PNG "") 

8.	Añade el fichero tunombre.md en el que se muestre un listado de los módulos en los que estás matriculado.

9.	Crea un tag llamado v0.1 

10.	Sube los cambios al repositorio remoto 

11.	Por último, crea una tabla en el documento anterior en el que se muestre el nombre de 2 compañeros y su enlace al repositorio en GitLab o GitHub.

|Alumno                   | Repositorio                                      |
|:------------------------|:-------------------------------------------------|
|Paula			          | https://gitlab.com/PRGGR/paula_markdown          | 
|Carlos       			  | https://gitlab.com/Carlosangel17/carlos_markdown | 


***
***

## **Hoja03 Markdown 03**

***

###A partir de la tarea anterior vamos a profundizar más en el uso de Git:

1. Creación de ramas:
	*	Crea la rama rama-TUNOMBRE 
	*	Posiciona tu carpeta de trabajo en esta rama 
	![imagen](imagenes/Hoja03/1-1.PNG "") 

2.	Añade un fichero y crea la rama remota
	*	Crea un fichero llamado daw.md con únicamente una cabecera DESARROLLO DE 
APLICACIONES WEB 
	*	Haz un commit con el mensaje “Añadiendo el archivo daw.md en la rama-TUNOMBRE” 
	![imagen](imagenes/Hoja03/2-1.PNG "") 

	*	Sube los cambios al repositorio remoto. NOTA: date cuenta que ahora se deberá hacer 
con el comando **git push origin rama-TUNOMBRE**
	![imagen](imagenes/Hoja03/2-2.PNG "") 

3.	Haz un merge directo
	*	Posiciónate en la rama master 
	![imagen](imagenes/Hoja03/3-1.PNG "") 
	*	Haz un merge de la rama-TUNOMBRE en la rama master 
	![imagen](imagenes/Hoja03/3-2.PNG "") 

4.	Haz un merge con conflicto
	*	En la rama master añade al fichero daw.md una tabla en la que muestres los módulos
del primer curso de DAW. 
	*	Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW1” 
	![imagen](imagenes/Hoja03/4-1.PNG "") 
	*	Posiciónate ahora en la rama-TUNOMBRE 
	![imagen](imagenes/Hoja03/4-2.PNG "") 
	*	Escribe en el fichero daw.md otra tabla con los módulos del segundo curso de DAW 
	*	Añade los archivos y haz un commit con el mensaje “Añadida tabla DAW2” 
	![imagen](imagenes/Hoja03/4-3.PNG "") 
	*	Posiciónate otra vez en master y haz un merge con la rama-TUNOMBRE 
	![imagen](imagenes/Hoja03/4-4.PNG "") 
	![imagen](imagenes/Hoja03/4-5.PNG "") 

5.	Arreglo del conflicto
	*	Arregla el conflicto editando el fichero daw.md y haz un commit con el mensaje 
“Finalizado el conflicto de daw.md” 
	![imagen](imagenes/Hoja03/5-2.PNG "") 
	![imagen](imagenes/Hoja03/5-3.PNG "") 
	![imagen](imagenes/Hoja03/5-1.PNG "") 

6.	Tag y borrar la rama 
	*	Crea un tag llamado v0.2 
	![imagen](imagenes/Hoja03/6-1.PNG "") 
	*	Borra la rama-TUNOMBRE 
	![imagen](imagenes/Hoja03/6-2.PNG "") 

7.	Documenta todo y finaliza el ejercicio 
	*	En el fichero README.md crea una nueva sección en la que vayas documentando todo 
lo que vas realizando en esta tarea. 
	*	Documenta todos los puntos en el README.md, haz un commit y sube los cambios al
servidor 
	![imagen](imagenes/Hoja03/7-1.PNG "") 

	*	Haz un último commit y sube todo al servidor 

